var models = require('../models');
var eventproxy = require('eventproxy');
var Part = models.Part;

exports.getPartByPartName = function (part_name, callback) {
  Part.findOne({name: part_name}, callback);
};

//exports.getTopic = function (id, callback) {
//  Topic.findOne({_id: id}, callback);
//};

exports.newAndSave = function (name, topic_id, content, complex_whole, callback) {
  var part = new Part();
  part.name = name;
  part.content = content;
  part.topic_id = topic_id;
  part.complex_to_whole = complex_whole;
  part.save(callback);
};
