# DUMP to folder backup
mongodump --host 10.86.17.121:27017 -d node_club_dev -o backup

# Tar to a zip file
tar zcfv /git/nodeclub/backup.tar.gz backup

cd /git/nodeclub
git add backup.tar.gz
git commit --message="Update db data."





#############################

# Restore to data, example: db is in /data folder.
rm -rf ~/backup
tar zxfv /git/nodeclub/backup.tar.gz ~/backup

mongorestore --dbpath /data ~/backup

# Mac OS
mongorestore --dbpath /usr/local/var/mongodb ~/backup

