var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var utility = require('utility');

var PartSchema = new Schema({
  name: { type: String},

  content: { type: String },

  modified_at: { type: Date, default: Date.now },

  topic_id: { type: String},  // Min complex_to_whole topic.
  complex_to_whole: { type: Number, default: -1}  // -1 = Can not complex to whole
});

var PartFinishSchema = new Schema({
  part_name: { type: String },
  user_id: { type: String },
  finish_at: { type: Date, default: Date.now }
});

//UserSchema.virtual('isAdvanced').get(function () {
//  return this.score > 700 || this.is_star;
//});

PartSchema.index({name: 1}, {unique: true});
PartFinishSchema.index({part_name: 1, user_id: 1}, {unique: true});

mongoose.model('Part', PartSchema);
mongoose.model('PartFinish', PartFinishSchema);
