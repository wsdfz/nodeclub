var EventProxy=require('eventproxy');
var ep = new EventProxy();

var sys = require('sys');

ep.after('test1', 2, function(d) {
    sys.puts('Hit 1. ' + d);
});

ep.emit('test1', 'I am 1');
sys.puts('1');

ep.after('test1', 2, function(d) {
    sys.puts('Hit 2. ' + d);
});

ep.emit('test1', 'I am 2');
sys.puts('2');
ep.emit('test1', 'I am 3');
sys.puts('3');
ep.emit('test1', 'I am 4');
sys.puts('4');
//
//http.createServer(function(req, res) {
//    res.end('Hello, world');
//}).listen(1234);